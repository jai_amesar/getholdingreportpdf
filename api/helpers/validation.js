var validators = function (req, res, next) {
    var params = req.body;
    
    if (params.reportType) {
        if ((params.reportType.toLowerCase()).indexOf["pdf", "data"] != -1) {
            if (params.reportType.toLowerCase() == "pdf") {
                if (params.passwordProtectPDF) {
                    if ((params.passwordProtectPDF.toLowerCase()).indexOf["true", "false"] != -1) {
                        if (params.passwordProtectPDF.toLowerCase() == "true") {
                            if (params.pdfPassword && params.encryption) {
                                if ((params.encryption.toLowerCase()).indexOf["true, false"] != -1) {
                                    next();
                                } else {
                                    res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: undefined, ErrorDescription: "Invalid value for encryption" } });
                                }
                            } else {
                                res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: undefined, ErrorDescription: "Missing pdfPassword or encryption flag" } });
                            }
                        } else {
                            next();
                        }
                    } else {
                        res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: undefined, ErrorDescription: "Invalid value for passwordProtectPDF" } });
                    }
                } else {
                    next();
                }
            } else {
                next();
            }
        } else {
            res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: undefined, ErrorDescription: "Invalid value for reportType" } });
        }
    } else {
        res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: undefined, ErrorDescription: "Please specify reportType" } });
    }
}

module.exports = {
    validators: validators
};