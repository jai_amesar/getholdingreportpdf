// Maps swagger parameters to its original values
var map = (req) =>
    Object.keys(req.swagger.params).reduce((prev, curr) => {
        prev[curr] = req.swagger.params[curr].value;
        return prev;
    }, {});

module.exports = { map: map };