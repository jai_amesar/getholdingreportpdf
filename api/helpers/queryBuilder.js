// Helper to build an SQL query from JSON request parameters
var builder = require('mongo-sql');
var queryBuilder = function (params, type, view) {
    var queryObj = {
        type: type
        , table: view
    };

    if (type == "select" && params.filter) {
        queryObj["where"] = params.filter;
    }

    if (params.select) {
        queryObj["columns"] = params.select.split(",")
    }

    var query = builder.sql(queryObj);

    query.values     // Array of values
    query.toString()

    query.query = query.query.split("$").join(":");
    query.query = query.query.split("\"").join("");

    var dateRegex = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

    query.values = query.values.map(val => {
        if (dateRegex.test(val)) {
            val = new Date(Date.UTC(new Date(val).getFullYear(), new Date(val).getMonth(), new Date(val).getDate(), new Date(val).getHours(), new Date(val).getMinutes(), new Date(val).getSeconds()))
        }
        return val;
    })

    return query;
}

module.exports = queryBuilder;