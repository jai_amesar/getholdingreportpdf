let queryBuilder = require("../helpers/queryBuilder");
let oracledb = require('oracledb');
let async = require("async");
let path = require("path");
let moment = require("moment");
let ejs = require("ejs");
var ABFLHeader = require("../../ABFLHeader").header;
var ABFLFooter = require("../../ABFLFooter").footer;
const pdf2base64 = require('pdf-to-base64');
const HummusRecipe = require('hummus-recipe');
const crypto = require('crypto');
var fs = require("fs");

const { v4: uuidv4 } = require('uuid');

var logger = require("../helpers/log4j").logger;
var GeneratePDF = require("./GeneratePDF");

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.fetchArraySize = 200;

// Loan Pledge Details View API
let getHoldingReport = function (req, res) {
    try {
        let uuid = uuidv4();

        logger.info(uuid, "getHoldingReport ", "Request ", JSON.stringify(req.body));

        let params = req.body;

        if (params && params.filter) {
            if (!params.filter.AGREEMENTNO || 0 === params.filter.AGREEMENTNO.length) {
                throw new Error('Agreement no. is missing');
            }
        }
        else {
            throw new Error('filter is missing');
        }

        let view = process.env.DATAMART_SCHEMA + "." + process.env.LOAN_PLEDGE_DETAILS;
        let schemaUser = process.env.DATAMART_USER;
        let dateFields = ["PLEDGE_EXPIRY_DATE", "CBD"];
        let reportType = params.reportType;
        let encryption = params.encryption;
        let PDFPassword = params.pdfPassword;

        params.filter = findAndReplaceProp(params.filter, 'gte', '$gte');
        params.filter = findAndReplaceProp(params.filter, 'lte', '$lte');
        params.filter = findAndReplaceProp(params.filter, 'gt', '$gt');
        params.filter = findAndReplaceProp(params.filter, 'lt', '$lt');

        commonProcessing(params, view, schemaUser, uuid).then((data) => {
            var pdfData = "";
            async.eachSeries(data["jsonData"], (datum, datumCallback) => {
                async.eachSeries(dateFields, (date, dateCallback) => {
                    if (datum[date]) {
                        datum[date] = new Date(datum[date]).toLocaleString();
                    }
                    dateCallback();
                }, function () {
                    datumCallback();
                })
            }, function () {
                if (reportType.toLowerCase() == "pdf") {
                    if (data["accInfoData"].length && (data["reportDataForEquity"].length || data["reportDataForNonEquity"].length)) {
                        ejs.renderFile(path.join(__dirname, '../../views/', 'HoldingReport.ejs'), {
                            reportDataForEquity: data["reportDataForEquity"],
                            reportDataForNonEquity: data["reportDataForNonEquity"],
                            accInfoData: data["accInfoData"],
                            ABFLHeader: ABFLHeader,
                            ABFLFooter: ABFLFooter

                        }, (err, ejsData) => {

                            if (err) {
                                throw new Error(err);
                            }
                            else {
                                var pdfNameWop = uuid + '_wop.pdf';
                                var pdfName = uuid + '.pdf';

                                GeneratePDF.printPdf(ejsData, pdfNameWop).then(() => {
                                    var pdfFilePathWop = "./" + pdfNameWop;
                                    var pdfFilePath = "./" + pdfName;
                                    var pdfFilePathForBase64 = pdfFilePathWop;

                                    // Password protect generated PDF
                                    if (params.passwordProtectPDF && !!params.passwordProtectPDF) {
                                        if (encryption && !!encryption) {
                                            PDFPassword = decrypt(PDFPassword);
                                        }
                                        const pdfDoc = new HummusRecipe(path.join(__dirname + '../../../' + pdfNameWop), path.join(__dirname + '../../../' + pdfName));

                                        pdfDoc
                                            .encrypt({
                                                userPassword: PDFPassword,
                                                ownerPassword: '456',
                                                userProtectionFlag: 4
                                            })
                                            .endPDF();
                                        pdfFilePathForBase64 = pdfFilePath;
                                    }

                                    pdf2base64(pdfFilePathForBase64)
                                        .then(
                                            (response) => {
                                                pdfData = response;
                                                logger.info(uuid, "getHoldingReport ", "Response ", JSON.stringify({ "dataLength": data.length, "PDFData": true }));
                                                res.json({ getHoldingReport: data["jsonData"], status: { Status: "Success", Code: 200 }, PDFData: pdfData })
                                                deleteFile(params, pdfName, uuid, pdfNameWop);
                                            }
                                        )
                                        .catch(
                                            (error) => {
                                                logger.error(uuid, "getHoldingReport", "Error", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                                                res.status(400).json({ getHoldingReportReport: [], status: { Status: "Error", Code: 400, DBErrorCode: error ? error.errorNum : undefined, ErrorDescription: error ? error.message : undefined } });
                                                deleteFile(params, pdfName, uuid, pdfNameWop);
                                            }
                                        )

                                }).catch(error => {
                                    logger.error(uuid, "getDemandAdvice ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                                    res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: error ? error.errorNum : undefined, ErrorDescription: error ? error.message : undefined } });
                                });
                            }
                        });
                    }
                    else {
                        logger.info(uuid, "getHoldingReport ", "Response ", JSON.stringify({ "dataLength": data.length, "PDFData": null }));
                        res.status(200).json({ getHoldingReport: data["jsonData"], status: { Status: "Success", Code: 200 }, PDFData: null })
                    }

                }
                else if (reportType.toLowerCase() == "data") {
                    logger.info(uuid, "getHoldingReport ", "Response ", JSON.stringify({ "dataLength": data.length, "PDFData": null }));
                    res.status(200).json({ getHoldingReport: data["jsonData"], status: { Status: "Success", Code: 200 }, PDFData: null });
                }

            })
        })
            .catch(err => {
                logger.error(uuid, "getDemandAdvice ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: err ? err.errorNum : undefined, ErrorDescription: err ? err.message : undefined } });
            })
    }
    catch (err) {
        logger.error(uuid, "getHoldingReport ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
        res.status(400).json({ getHoldingReport: [], status: { Status: "Error", Code: 400, DBErrorCode: err ? err.errorNum : undefined, ErrorDescription: err ? err.message : undefined } });
    }
}
// Common processing module that builds the SQL query from the JSON filter
function commonProcessing(params, view, schemaUser, uuid) {
    return new Promise((resolve, reject) => {
        async.waterfall([
            function (outerCB) {
                async.parallel({
                    // Query Loan Acc Info view for RM Name
                    accountInfoData: function (cb) {
                        makeOracleConnection(schemaUser, uuid).then(connection => {
                            var accountParams = {
                                filter: {
                                    CIF_NO: params.filter["AGREEMENTNO"]
                                }
                            };
                            let query = queryBuilder(accountParams, 'select', process.env.CUSTOMER_ACCOUNT_INFO);
                            var startTime = Date.now();
                            logger.info(uuid, "getHoldingReport ", "Query ", JSON.stringify({ "query": JSON.stringify(query.query), "value": JSON.stringify(query.values) }));
                            connection.execute(query.query, query.values, (err, result) => {
                                if (err) {
                                    connection.close();
                                    cb(err);
                                } else {
                                    let endTime = Date.now();
                                    var responseTime = ((endTime - startTime) / 1000);
                                    logger.info(uuid, "getHoldingReport ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                    connection.close();
                                    cb(null, result.rows);
                                }
                            })
                        }).catch(err => {
                            reject(err);
                        })
                    },

                    // Query Original MR view for JSON data
                    jsonData: function (cb) {
                        makeOracleConnection(schemaUser, uuid).then(connection => {
                            let query = queryBuilder(params, 'select', view);
                            var startTime = Date.now();
                            logger.info(uuid, "getHoldingReport ", "Query ", JSON.stringify({ "query": JSON.stringify(query.query), "value": JSON.stringify(query.values) }));
                            connection.execute(query.query, query.values, (err, result) => {
                                if (err) {
                                    connection.close();
                                    cb(err);
                                } else {
                                    let endTime = Date.now();
                                    var responseTime = ((endTime - startTime) / 1000);
                                    logger.info(uuid, "getHoldingReport ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                    connection.close();
                                    cb(null, result.rows);
                                }
                            })
                        }).catch(err => {
                            reject(err);
                        })
                    },
                }, function (err, parallelOneResults) {
                    if (err) {
                        outerCB(err);
                    } else {
                        if (parallelOneResults["accountInfoData"].length <= 0) {
                            outerCB(new Error('No data found for Account Info view'));
                        }
                        else {
                            outerCB(null, parallelOneResults);
                        }
                    }
                })
            },

            function (parallelOneResults, outerCB) {
                let pledgeInfoData = parallelOneResults["jsonData"];
                let accountInfoData = parallelOneResults["accountInfoData"];

                async.parallel({
                    // Query new MR view for Report Equity Table
                    equityData: function (cb) {
                        makeOracleConnection(schemaUser, uuid).then(connection => {
                            let query = `select * from ABFL_DATAMART_CR.VW_LOAN_ACCOUNT_PLEDGE_INFO_NEW where 
                                                        CUSTOMERID = ${accountInfoData[0].CUSTOMERID} and 
                                                        AGREEMENTNO = '${accountInfoData[0].CIF_NO}' and 
                                                        COLLATERALTYPE = 'EQUITY' `;
                            var startTime = Date.now();

                            let queryForLog = "select * from ABFL_DATAMART_CR.VW_LOAN_ACCOUNT_PLEDGE_INFO_NEW where CUSTOMERID = :1 and AGREEMENTNO = :2 and COLLATERALTYPE = :3 ";
                            let value = " [ "+accountInfoData[0].CUSTOMERID + " , '"+accountInfoData[0].CIF_NO+"', 'EQUITY' ]";

                            logger.info(uuid, "getHoldingReport ", "Query ", JSON.stringify({ "query": JSON.stringify(queryForLog), "value": JSON.stringify(value) }));
                            connection.execute(query, (err, result) => {
                                if (err) {
                                    connection.close();
                                    cb(err);
                                } else {
                                    let endTime = Date.now();
                                    var responseTime = ((endTime - startTime) / 1000);
                                    logger.info(uuid, "getHoldingReport ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                    connection.close();
                                    cb(null, result.rows);
                                }
                            })
                        }).catch(err => {
                            reject(err);
                        })
                    },

                    // Query new MR view for Report Non-Equity Table
                    nonEquityData: function (cb) {
                        makeOracleConnection(schemaUser, uuid).then(connection => {
                            let query = `select * from ABFL_DATAMART_CR.VW_LOAN_ACCOUNT_PLEDGE_INFO_NEW where 
                                                        CUSTOMERID = ${accountInfoData[0].CUSTOMERID} and 
                                                        AGREEMENTNO = '${accountInfoData[0].CIF_NO}' and 
                                                        COLLATERALTYPE = 'NON-EQUITY' and
                                                        TYPE_OF_NON_EQUITY_SECURITY = 'Property'`;
                            var startTime = Date.now();
                            let queryForLog = "select * from ABFL_DATAMART_CR.VW_LOAN_ACCOUNT_PLEDGE_INFO_NEW where CUSTOMERID = :1 and AGREEMENTNO = :2 and COLLATERALTYPE = :3 and TYPE_OF_NON_EQUITY_SECURITY = :4";
                            let value = " [ "+accountInfoData[0].CUSTOMERID + " , '"+accountInfoData[0].CIF_NO+"', 'NON-EQUITY', 'Property']";
                            logger.info(uuid, "getHoldingReport ", "Query ", JSON.stringify({ "query": JSON.stringify(queryForLog), "value": JSON.stringify(value) }));
                            connection.execute(query, (err, result) => {
                                if (err) {
                                    connection.close();
                                    cb(err);
                                } else {
                                    let endTime = Date.now();
                                    var responseTime = ((endTime - startTime) / 1000);
                                    logger.info(uuid, "getHoldingReport ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                    connection.close();
                                    cb(null, result.rows);
                                }
                            })
                        }).catch(err => {
                            reject(err);
                        })
                    },
                }, function (err, parallelTwoResults) {
                    if (err) {
                        outerCB(err);
                    } else {
                        outerCB(null, {
                            jsonData: pledgeInfoData,
                            accInfoData: accountInfoData,
                            reportDataForEquity: parallelTwoResults.equityData,
                            reportDataForNonEquity: parallelTwoResults.nonEquityData
                        })
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                reject(err);
            }
            else {
                resolve(results);
            }
        })
    })
}

// Oracle connection module
function makeOracleConnection(schemaUser, uuid) {
    return new Promise((resolve, reject) => {
        let connectionDetails = {
            user: schemaUser,
            password: process.env.ORACLE_PASSWORD,
            connectString: process.env.ORACLE_CONNECTION_STRING
        }
        oracledb.getConnection(connectionDetails,
            function (err, connection) {
                if (err) {
                    reject(err);
                }
                else {
                    logger.info(uuid, "getHoldingReport ", "Success ", JSON.stringify({ 'successMessage': "Application started -> Connected to DB" }));
                    resolve(connection);
                }
            });
    })
}

function deleteFile(params, pdfName, uuid, pdfNameWop) {
    if (params.passwordProtectPDF && !!params.passwordProtectPDF) {
        fs.unlink(pdfName, (err) => {
            if (err) {
                logger.error(uuid, "getDemandAdvice ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                return;
            }
        });
    }

    fs.unlink(pdfNameWop, (err) => {
        if (err) {
            logger.error(uuid, "getDemandAdvice ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
            return;
        }
    });
}

// Password decryption utility
function decrypt(text) {
    try {
        let encryptedText = Buffer.from(text, 'base64');
        let decipher = crypto.createDecipheriv("aes-256-ecb", Buffer.from(process.env.ENCRYPTION_KEY), '');
        let decrypted = decipher.update(encryptedText);

        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }
    catch (err) {
        throw new Error(err);
    }
}

// Find and replace gte/lte with $gte/$lte
function findAndReplaceProp(obj, key, replaceKey, out) {
    var i,
        proto = Object.prototype,
        ts = proto.toString,
        hasOwn = proto.hasOwnProperty.bind(obj);

    if ('[object Array]' !== ts.call(out)) out = [];

    for (i in obj) {
        if (hasOwn(i)) {
            if (i.toLowerCase() === key) {
                obj[replaceKey] = obj[i];
                delete obj[i];
            } else if ('[object Array]' === ts.call(obj[i]) || '[object Object]' === ts.call(obj[i])) {
                findAndReplaceProp(obj[i], key, replaceKey, out);
            }
        }
    }

    return obj;
}

// Health API for Kubernetes
let healthApi = function (req, res) {
    res.status(200).send("Alive")
}

module.exports = {
    healthApi: healthApi,
    getHoldingReport: getHoldingReport
}