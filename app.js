'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var bodyParser = require("body-parser");
var swaggerUi = require("swagger-ui-express");
var YAML = require("yamljs");
var validators = require("./api/helpers/validation").validators;

var swaggerDocument = YAML.load("./api/swagger/swagger.yaml");

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(validators);

module.exports = app; // for testing

app.set('view engine', 'ejs');

app.use('/api/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 80;
  var server = app.listen(port);
  server.timeout = 300000;
});
